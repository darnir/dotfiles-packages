# Environment variables set by pam(8) for a user session.
#
# This file fixes the variables responsible for making the XDG basedir spec work
# (https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html)
# and does not set any other environment variables, as it is rather cumbersome
# to adopt changes herein. Other environment variables are set in the shell
# scripts' respective startup files (.zprofile, .bash_profile, ...).

# Some environment variables that should be set early
TZ                              DEFAULT="Europe/Berlin"

# Set XDG Base Directory Paths
XDG_CACHE_HOME                  DEFAULT="@{HOME}/.local/var/cache"
XDG_CONFIG_HOME                 DEFAULT="@{HOME}/.local/etc"
XDG_DATA_HOME                   DEFAULT="@{HOME}/.local/var/lib"
XDG_RUNTIME_DIR                 DEFAULT="@{HOME}/.local/run"
XDG_LOG_HOME                    DEFAULT="@{HOME}/.local/var/log"
XDG_LIB_HOME                    DEFAULT="@{HOME}/.local/lib"
XDG_BIN_HOME                    DEFAULT="@{HOME}/.local/bin"
LD_LIBRARY_PATH                 DEFAULT="${XDG_LIB_HOME}"

# Fix applications to follow XDG Spec
CARGO_HOME                      DEFAULT="${XDG_DATA_HOME}/cargo"
RUSTUP_HOME                     DEFAULT="${XDG_DATA_HOME}/rustup"
PASSWORD_STORE_DIR              DEFAULT="${XDG_DATA_HOME}/pass"
WEECHAT_HOME                    DEFAULT="${XDG_CONFIG_HOME}/weechat"
WGETRC                          DEFAULT="${XDG_CONFIG_HOME}/wget/wgetrc"
WGET2RC                         DEFAULT="${XDG_CONFIG_HOME}/wget/wget2rc"
WINEPREFIX                      DEFAULT="${XDG_DATA_HOME}/wine/default"
XAUTHORITY                      DEFAULT="${XDG_RUNTIME_DIR}/Xauthority"
ZDOTDIR                         DEFAULT="${XDG_CONFIG_HOME}/zsh"
ADOTDIR                         DEFAULT="${XDG_DATA_HOME}/antigen"
NOTMUCH_CONFIG                  DEFAULT="${XDG_CONFIG_HOME}/notmuch/config"
CCACHE_CONFIGPATH               DEFAULT="${XDG_CONFIG_HOME}/ccache/ccache.conf"
INPUTRC                         DEFAULT="${XDG_CONFIG_HOME}/sh/inputrc"
TASKRC                          DEFAULT="${XDG_CONFIG_HOME}/task/config"


# Set Environment for VDPAU
# VDPAU_DRIVER                    DEFAULT="radeonsi"
QT_QPA_PLATFORMTHEME		DEFAULT="qt5ct"
