#!/usr/bin/env sh

# Install the githooks

for hook in .githooks/*; do
	ln -vs $(pwd)/$hook .git/hooks/
done
