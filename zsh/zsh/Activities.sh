#!/usr/bin/env sh

# Define command line activities
# When in a KDE session this script integrates with KDE Activities and allows
# easy switching between them.
# In other cases, this script will allow switchign between predefined
# activities on the command line.


# KDE Activity Management
#
# This block of code here handles KDE Activity switching for the console. It
# detects the current Activity and then sets the shell prompt to display it. A
# specific per-activity config file is also sourced which can be used to set up
# custom aliases, functions or environment for that particular shell

if [ "$KDE_FULL_SESSION" = true ]; then
	ACTIVITY_MANAGER="KDE"
	. ${XDG_CONFIG_HOME}/zsh/KDE_Manager.zsh
else
	ACTIVITY_MANAGER="Console"
	ACT_FILE="${XDG_DATA_HOME}/zsh/activity.sh"
fi

# Set the Current Activity on loading
if [ "$ACTIVITY_MANAGER" = "KDE" ]; then
	CUR_ACT=$(qdbus org.kde.ActivityManager /ActivityManager/Activities org.kde.ActivityManager.Activities.ActivityName "$(qdbus org.kde.ActivityManager /ActivityManager/Activities org.kde.ActivityManager.Activities.CurrentActivity)")
else
	[ ! -f "${ACT_FILE}" ] && echo "export CUR_ACT=Default" > "${ACT_FILE}"
	. "${ACT_FILE}"
fi

mkdir -p "${XDG_DATA_HOME}/zsh/$CUR_ACT"
HISTFILE=${XDG_DATA_HOME}/zsh/$CUR_ACT/zhistfile
activity="[%{$fg[yellow]%}$CUR_ACT%{$reset_color%}]"
[ -f "${XDG_CONFIG_HOME}/zsh/$CUR_ACT.zsh" ] && . "${XDG_CONFIG_HOME}/zsh/$CUR_ACT.zsh"

sact() {
	if [ "$ACTIVITY_MANAGER" = "KDE" ]; then
		kact "$1"
	else
		echo "export CUR_ACT=$1" >! "$ACT_FILE"
	fi
}
