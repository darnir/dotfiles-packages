# zmodload zsh/zprof
. ${XDG_CONFIG_HOME}/zsh/zsh_main.zsh

# History Settings
HISTFILE=${XDG_DATA_HOME}/zsh/zhistfile
HISTSIZE=10000
SAVEHIST=10000
setopt append_history	# Append history to file instead of replacing it
setopt hist_ignore_dups	# Ignore duplicate entries in history
setopt hist_fcntl_lock	# Use fcntl to lock history file
setopt hist_ignore_space	# Ignore lines that begin with whitespace
setopt hist_reduce_blanks	# Reduce blank spaces

# Change Directories
setopt autocd		# Automatically cd to directory if possible
setopt auto_pushd	# Automatically push directory on stack during cd

setopt hash_cmds hash_dirs

setopt HIST_IGNORE_DUPS

setopt auto_remove_slash        # self explicit
setopt chase_links              # resolve symlinks
setopt correct                  # try to correct spelling of commands
setopt extended_glob            # activate complex pattern globbing
setopt glob_dots                # include dotfiles in globbing
unsetopt beep                   # no bell on error
unsetopt bg_nice                # no lower prio for background jobs
unsetopt clobber                # must use >| to truncate existing files
unsetopt hist_beep              # no bell on error in history
unsetopt ignore_eof             # do not exit on end-of-file
unsetopt list_beep              # no bell on ambiguous completion
unsetopt rm_star_silent         # ask for confirmation for `rm *' or `rm path/*'
print -Pn "\e]0; %n@%M: %~\a"   # terminal title

# Load other config files
[[ -a ${XDG_CONFIG_HOME}/sh/aliases ]] && . ${XDG_CONFIG_HOME}/sh/aliases

autoload -U promptinit colors
promptinit && colors
setopt PROMPT_SUBST

# Git parts of the prompt
autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' stagedstr '%F{green}●'
zstyle ':vcs_info:*' unstagedstr '%F{yellow}●'

precmd() {
    if [[ -z $(git ls-files --other --exclude-standard 2> /dev/null) ]]; then {
        zstyle ':vcs_info:*' actionformats '%c%u %F{yellow}%s%F{white} - %F{cyan}%r%F{white}|%F{cyan}%b%F{white}|%F{red}%a%F{white}'
        zstyle ':vcs_info:*' formats '%c%u %F{yellow}%s%F{white} - %F{green}%r%F{white}|%F{cyan}%b%F{white}'
    }
    else {
        zstyle ':vcs_info:*' actionformats '%c%u%F{red}● %F{yellow}%s%F{white} - %F{cyan}%r%F{white}|%F{cyan}%b%F{white}|%F{red}%a%F{white}'
        zstyle ':vcs_info:*' formats '%c%u%F{red}● %F{yellow}%s%F{white} - %F{green}%r%F{white}|%F{cyan}%b%F{white}'
    }
    fi
    vcs_info
}

# KDE Activity Management
#
# # This block of code here handles KDE Activity switching for the console. It
# # detects the current Activity and then sets the shell prompt to display it. A
# # specific per-activity config file is also sourced which can be used to set up
# # custom aliases, functions or environment for that particular shell
# if [[ $KDE_FULL_SESSION == true ]]; then
# 	CUR_ACT=$(qdbus org.kde.ActivityManager /ActivityManager/Activities org.kde.ActivityManager.Activities.ActivityName "$(qdbus org.kde.ActivityManager /ActivityManager/Activities org.kde.ActivityManager.Activities.CurrentActivity)")
# 	mkdir -p ${XDG_DATA_HOME}/zsh/$CUR_ACT/
# 	HISTFILE=${XDG_DATA_HOME}/zsh/$CUR_ACT/zhistfile
# 	kde_activity="[%{$fg[yellow]%}$CUR_ACT%{$reset_color%}]"
# 	[ -f ${XDG_CONFIG_HOME}/zsh/$CUR_ACT.zsh ] && . ${XDG_CONFIG_HOME}/zsh/$CUR_ACT.zsh
# 	. ${XDG_CONFIG_HOME}/zsh/KDE_Manager.zsh
# fi

# . ${XDG_CONFIG_HOME}/zsh/Activities.sh

# Actual prompt
PROMPT='[%{$fg[green]%}%n%{$fg[cyan]%}@%{$fg[blue]%}%m %{$fg[cyan]%}%3~%{$reset_color%}]%# '
RPROMPT='%(?..[%?] )${vcs_info_msg_0_}$vim_mode${activity}'

fpath=( ~/.local/var/lib/zsh/scripts $fpath )

source /usr/share/doc/pkgfile/command-not-found.zsh
source ${XDG_CONFIG_HOME}/zsh/zsh_plugins.zsh

[ -f /usr/share/fzf/key-bindings.zsh ] && . /usr/share/fzf/key-bindings.zsh
[ -f /usr/share/fzf/completion.zsh ] && . /usr/share/fzf/completion.zsh
[ -f ~/.local/etc/fzf/fzf.sh ] && . ~/.local/etc/fzf/fzf.sh
[ -f ${XDG_CONFIG_HOME}/sh/dir_colors ] && eval $(dircolors ${XDG_CONFIG_HOME}/sh/dir_colors)

bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
bindkey -M vicmd 'k' history-substring-search-up
bindkey -M vicmd 'j' history-substring-search-down
# zprof
