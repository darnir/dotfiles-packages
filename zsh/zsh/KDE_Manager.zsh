#!/usr/bin/env zsh

kprint_activities() {
	for act in $(qdbus org.kde.ActivityManager /ActivityManager/Activities org.kde.ActivityManager.Activities.ListActivities | xargs); do
		name=$(qdbus org.kde.ActivityManager /ActivityManager/Activities org.kde.ActivityManager.Activities.ActivityName "$act")
		echo "$name $act"
	done
}

KDE_ACTIVITY_GAMING="47939a61-9d45-44d3-860e-5fa1c9e387ec"
KDE_ACTIVITY_OS="495b07a7-b0fe-4282-a931-91d4ab8e76bf"
KDE_ACTIVITY_WORK="e3c1d5f6-4133-49fa-bcce-85d6e414b718"
KDE_ACTIVITY_PROGRAMMING="f51238fc-78ec-4f1b-bbc7-882b5e8ad864"
KDE_ACTIVITY_VRTS="13c4ab47-4585-4eb2-b426-eded6d0654d5"
KDE_ACTIVITY_DEFAULT="c0ed9dbd-2c42-4ed8-a1a2-bf60cd8c0d90"

kact() {
	local new_act="$1"
	# Create the variable name which will hold the activity ID for the given
	# activity name
	local k_name="KDE_ACTIVITY_$new_act:u"
	# Get the activity ID
	local _act=${(P)k_name}
	qdbus org.kde.ActivityManager /ActivityManager/Activities org.kde.ActivityManager.Activities.SetCurrentActivity "$_act"
}
