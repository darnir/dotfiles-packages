#!/usr/bin/env bash

set -x

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

primary=$(xrandr -q | grep "primary" | awk '{print $1}')

# Launch bar1 and bar2
MONITOR="$primary" polybar -c $XDG_CONFIG_HOME/polybar/config i3bar &

for i in $(xrandr -q | grep " connected " | grep -v "$primary" | awk '{print $1}' | xargs); do
	MONITOR="$i" polybar -c $XDG_CONFIG_HOME/polybar/config i3bar2 &
done
