let g:tex_flavor = "latex"
let g:tex_fold_enabled = 1
set conceallevel=2
let g:tex_comment_nospell = 1
