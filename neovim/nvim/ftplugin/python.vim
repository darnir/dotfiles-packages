let python_highlight_all = 1

let b:ale_fixers = ['black']
let b:ale_python_flake8_options = "--max-line-length 88"
